package com.example.bookreviews.domain;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.bookreviews.model.Frequency;
import com.example.bookreviews.model.Review;
import com.example.bookreviews.repository.ReviewRepository;
import com.google.common.collect.HashMultiset;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RelativeFrequencyCalculatorTests {
	
	@Autowired
	ReviewRepository repository;
	
	@Test
	public void testCalculateRelativeFrequency_LOADING_FROM_DB(){
		//Unit tests for this class should not load data from the database. 
		//This test is here just to make it easier for the evaluators to validate the implementation against real data
		RelativeFrequencyCalculator calculator = new RelativeFrequencyCalculator(500);
		
		List<Review> reviews = repository.findAll();
		
		calculator.calculateRelativeFrequency(reviews);
		
		reviews.forEach(review -> {
			List<Frequency> frequency = calculator.getRelativeFrequencyByReviewId(review.getId());
			
			assertNotNull(frequency);
			
			//The sum of the relative frequencies must be 1 (except if none of the words are in the most popular set)
			double frequencySum = frequency.stream().mapToDouble(Frequency::getWeight).sum();
			if(frequency.size() > 0 && frequencySum != 0.0d)
				assertTrue(Math.abs(1.0d - frequencySum) < 0.01);
		});
	}
	
	@Test
	public void testCalculateTop500_LOADING_FROM_DB(){
		//Unit tests for this class should not load data from the database. 
		//This test is here just to make it easier for the evaluators to validate the implementation against real data
		
		RelativeFrequencyCalculator calculator = new RelativeFrequencyCalculator(500);
		
		List<Review> reviews = repository.findAll();
		
		HashMap<String, Integer> frequencyMap = calculator.calculateMostPopularWords(reviews);
		assertTrue(frequencyMap.size() == 500);
	}
	
	@Test
	public void testCalculateRelativeFrequency(){
		RelativeFrequencyCalculator calculator = new RelativeFrequencyCalculator(500);
		
		Review review = getReview();
		
		List<Review> reviews = new ArrayList<Review>();
		reviews.add(review);
		
		calculator.calculateRelativeFrequency(reviews);
		List<Frequency> frequency = calculator.getRelativeFrequencyByReviewId(review.getId());
		
		assertNotNull(frequency);
		assertTrue(frequency.size() <= 39);
		
		//The sum of the relative frequencies must be 1 (except if none of the words are in the most popular set)
		double frequencySum = frequency.stream().mapToDouble(Frequency::getWeight).sum();
		assertTrue(Math.abs(1.0d - frequencySum) < 0.01);
	}
	
	@Test
	public void testCalculateTopWords(){
		RelativeFrequencyCalculator calculator = new RelativeFrequencyCalculator(10);
		
		Review review = getReview();
		
		List<Review> reviews = new ArrayList<Review>();
		reviews.add(review);
		
		HashMap<String, Integer> frequencyMap = calculator.calculateMostPopularWords(reviews);
		assertTrue(frequencyMap.size() == 10);
	}
	
	@Test
	public void testCalculateFrequency(){
		RelativeFrequencyCalculator calculator = new RelativeFrequencyCalculator(500);
		
		Review review = getReview();	
		
		HashMultiset<String> frequencyMap = calculator.calculateWordsFrequency(review);
		assertTrue(frequencyMap.size() == 48);
		assertTrue(frequencyMap.entrySet().size() == 39);
		assertTrue(frequencyMap.count("the") == 5);
	}
	
	private Review getReview(){
		return new Review(1, 5.0f, "/gp/customer-reviews/R3GFO6M9HJB5KZ?ASIN=1491590173", "Science is great, writing is fair.", "<span class=\"a-size-base review-text\">Sorry...i really wanted to like this book based on the reviews. The science pieces were indeed believable and well reasoned. What i didn't like is the actual style of the prose. The contrast of highly cerebral science to high-school level writing made for an awkward read.</span>");
	}
}
