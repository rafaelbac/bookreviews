package com.example.bookreviews.model;

public class Review {
	private long id;
	private float score;
	private String urlTail;
	private String title;
	private String text;
	
	public Review() {
		super();
	}

	public Review(long id, float score, String urlTail, String title, String text) {
		super();
		this.id = id;
		this.score = score;
		this.urlTail = urlTail;
		this.title = title;
		this.text = text;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public String getUrlTail() {
		return urlTail;
	}
	public void setUrlTail(String urlTail) {
		this.urlTail = urlTail;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
}
