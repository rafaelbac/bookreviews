package com.example.bookreviews.model;

public class Frequency {
	private long reviewId;
	private String word;
	private int count;
	private float weight; 
	
	public Frequency() {
		super();
	}

	public Frequency(long reviewId, String word, int count) {
		super();
		this.reviewId = reviewId;
		this.word = word;
		this.count = count;
	}
	
	public Frequency(long reviewId, String word, int count, float weight) {
		super();
		this.reviewId = reviewId;
		this.word = word;
		this.count = count;
		this.weight = weight;
	}

	public long getReviewId() {
		return reviewId;
	}

	public void setReviewId(long reviewId) {
		this.reviewId = reviewId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}
}
