package com.example.bookreviews.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DatabaseConfiguration {
	
	@Bean(name = "postgresDataSource")
	public DataSource dataSource() throws SQLException {
	    HikariConfig config = new HikariConfig("/hikari.properties");
	    HikariDataSource dataSource = new HikariDataSource(config);

	    return dataSource;
	}
}
