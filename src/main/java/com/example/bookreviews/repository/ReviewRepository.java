package com.example.bookreviews.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.bookreviews.model.Review;

@Repository
public class ReviewRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Review> findAll(){
		return jdbcTemplate.query("SELECT id, score, url_tail, title, text FROM public.reviews;", new ReviewRowMapper());
	}
	
	public Review findOne(long id){
		return jdbcTemplate.queryForObject("SELECT id, score, url_tail, title, text FROM public.reviews where id = ?;", 
				new Object[]{id}, new ReviewRowMapper());
	}
	
	private static final class ReviewRowMapper implements RowMapper<Review> {
		@Override
		public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
			Review review = new Review();
            review.setId(rs.getLong("id"));
            review.setScore(rs.getFloat("score"));
            review.setUrlTail(rs.getString("url_tail"));
            review.setTitle(rs.getString("title"));
            review.setText(rs.getString("text"));
            return review;
		}
	} 
}
