package com.example.bookreviews.service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bookreviews.domain.RelativeFrequencyCalculator;
import com.example.bookreviews.dto.WordCount;
import com.example.bookreviews.model.Frequency;
import com.example.bookreviews.model.Review;
import com.example.bookreviews.repository.ReviewRepository;

@Service
public class ReviewServiceImpl implements ReviewService {
	
	private static final int MOST_POPULAR_COUNT = 500;
	
	@Autowired
	ReviewRepository reviewRepository;
	
	RelativeFrequencyCalculator calculator;

	@Override
	public List<Review> findAll(){
		return reviewRepository.findAll();
	}
	
	@Override
	public Review findOne(long id){
		return reviewRepository.findOne(id);
	}
	
	private void calculateFrequencyIfNeeded(){
		if(calculator == null){
			calculator = new RelativeFrequencyCalculator(MOST_POPULAR_COUNT); 
			List<Review> reviews = reviewRepository.findAll();
			calculator.calculateRelativeFrequency(reviews);
		}
	}
	
	@Override
	public List<WordCount> getMostPopularWords(){
		calculateFrequencyIfNeeded();
		
		HashMap<String, Integer> popularWordsMap = calculator.getMostPopularWordsMap();
		if(popularWordsMap != null){
			return popularWordsMap.entrySet().stream()
			.map(w -> new WordCount(w.getKey(), w.getValue()))
			.sorted((w1, w2) -> w2.getCount() - w1.getCount())
			.collect(Collectors.toList());
		}
		
		return null;
	}
	
	@Override
	public List<Frequency> getFrequencyByReviewId(long id){
		calculateFrequencyIfNeeded();
		
		return calculator.getRelativeFrequencyByReviewId(id);
	}
}
