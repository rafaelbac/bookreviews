package com.example.bookreviews.service;

import java.util.List;

import com.example.bookreviews.dto.WordCount;
import com.example.bookreviews.model.Frequency;
import com.example.bookreviews.model.Review;

public interface ReviewService {
	public List<Review> findAll();
	public Review findOne(long id);
	public List<WordCount> getMostPopularWords();
	public List<Frequency> getFrequencyByReviewId(long id);
}
