package com.example.bookreviews.domain;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;

import com.example.bookreviews.model.Frequency;
import com.example.bookreviews.model.Review;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.ConcurrentHashMultiset;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Multisets;

public class RelativeFrequencyCalculator {
	
	private int mostPopularCount;
	HashMap<String, Integer> mostPopularMap;
	ConcurrentHashMultiset<String> allReviewsWords;
	ConcurrentHashMap<Long, List<Frequency>> reviewWeightsMap;
	ConcurrentHashMap<Long, HashMultiset<String>> reviewWordsMap;
	
	public RelativeFrequencyCalculator(int mostPopularCount){
		this.mostPopularCount = mostPopularCount;
		mostPopularMap = new HashMap<String, Integer>();
		allReviewsWords = ConcurrentHashMultiset.create();
		reviewWeightsMap = new ConcurrentHashMap<Long, List<Frequency>>();
		reviewWordsMap = new ConcurrentHashMap<Long, HashMultiset<String>>();
	}
	
	public List<Frequency> getRelativeFrequencyByReviewId(long reviewId){
		return reviewWeightsMap.get(reviewId);
	}
	
	public HashMap<String, Integer> getMostPopularWordsMap(){
		return mostPopularMap;
	}
	
	public void calculateRelativeFrequency(List<Review> reviews){
		calculateMostPopularWords(reviews);
		
		reviews.parallelStream().forEach(review -> calculateRelativeFrequency(review));
	}
	
	protected void calculateRelativeFrequency(Review review){
		HashMultiset<String> reviewWords = reviewWordsMap.get(review.getId());
		if(reviewWords != null){
			final int reviewTopWordsFrequency = reviewWords.entrySet().stream()
					.filter(w -> mostPopularMap.containsKey(w.getElement()))
					.mapToInt(w -> w.getCount())
					.sum();

			List<Frequency> frequencyList = reviewWords.entrySet().stream()
			.filter(w -> mostPopularMap.containsKey(w.getElement()))
			.map(w -> new Frequency(review.getId(), w.getElement(), w.getCount(), 
					(float) w.getCount() / (float) reviewTopWordsFrequency))
			.collect(Collectors.toList());
			reviewWeightsMap.put(review.getId(), frequencyList);
		}
	}
	
	protected HashMap<String, Integer> calculateMostPopularWords(List<Review> reviews){
		reviews.parallelStream().forEach(review -> calculateWordsFrequency(review));
		
		ImmutableMultiset<String> sortedWords = Multisets.copyHighestCountFirst(allReviewsWords);
		sortedWords.entrySet().stream()
		.limit(mostPopularCount)
		.forEachOrdered(w -> mostPopularMap.put(w.getElement(), w.getCount()));
				
		return mostPopularMap;
	}
	
	protected HashMultiset<String> calculateWordsFrequency(Review review){	
		HashMultiset<String> reviewWords = HashMultiset.create();

		//Removing HTML tags
		String text = Jsoup.parse(review.getText())
				.text()
				.toLowerCase();
		
		//Replacing punctuation with white spaces
		text = CharMatcher.anyOf(".,;?!-").replaceFrom(text, ' ');
		
		//Removing special characters (considering that numbers are not words)
		text = CharMatcher.inRange('a', 'z')
				.or(CharMatcher.whitespace())
				.retainFrom(text);
		
		//Tokenizing and creating the frequency map
		Splitter splitter = Splitter.on(' ').omitEmptyStrings().trimResults();
		List<String> tokens = splitter.splitToList(text);
		reviewWords.addAll(tokens);
		allReviewsWords.addAll(tokens);

		reviewWordsMap.put(review.getId(), reviewWords);

		return reviewWords;
	}
}
