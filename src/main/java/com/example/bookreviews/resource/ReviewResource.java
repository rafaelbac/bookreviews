package com.example.bookreviews.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.bookreviews.dto.WordCount;
import com.example.bookreviews.model.Frequency;
import com.example.bookreviews.model.Review;
import com.example.bookreviews.service.ReviewService;

/**
 * Created by rafaelbacelar on 27/05/17.
 */

@RestController
@RequestMapping(value="/reviews")
public class ReviewResource {

	@Autowired
	private ReviewService reviewService;
	
	@RequestMapping(method=RequestMethod.GET, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<Review>> getReviews(){
		return new ResponseEntity<List<Review>>(reviewService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value="{id}",
			method=RequestMethod.GET, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Review> getReview(@PathVariable("id") Long id){
		if(id == null || id < 1)
			return new ResponseEntity<Review>(HttpStatus.BAD_REQUEST);
		return new ResponseEntity<Review>(reviewService.findOne(id), HttpStatus.OK);
	}
	
	@RequestMapping(value="popular-words",
			method=RequestMethod.GET, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<WordCount>> getMostPopularWords(){
		return new ResponseEntity<List<WordCount>>(reviewService.getMostPopularWords(), HttpStatus.OK);
	}
	
	@RequestMapping(value="{id}/frequency",
			method=RequestMethod.GET, 
			produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<List<Frequency>> getFrequencyByReviewId(@PathVariable("id") Long id){
		if(id == null || id < 1)
			return new ResponseEntity<List<Frequency>>(HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<List<Frequency>>(reviewService.getFrequencyByReviewId(id), HttpStatus.OK);
	}
}
